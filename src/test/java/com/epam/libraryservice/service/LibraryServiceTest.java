package com.epam.libraryservice.service;

import com.epam.libraryservice.entity.UserBookData;
import com.epam.libraryservice.client.BookServiceClient;
import com.epam.libraryservice.client.UserServiceClient;
import com.epam.libraryservice.exception.DataNotFoundException;
import com.epam.libraryservice.exception.ServerException;
import com.epam.libraryservice.model.BookModel;
import com.epam.libraryservice.model.LibraryUserModel;
import com.epam.libraryservice.model.UserBookDataModel;
import com.epam.libraryservice.model.UserModel;
import com.epam.libraryservice.repository.LibraryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

class LibraryServiceTest {

   @Mock
   private LibraryRepository libraryRepository;

   @Mock
   private UserServiceClient userServiceClient;

   @Mock
   private BookServiceClient bookServiceClient;

   @InjectMocks
   private LibraryService libraryService;

   private Long userId = 1L;
   private Long bookId = 1L;
   private Long userBookDataId = 1L;
   private String emailId = "abc@gmail.com";
   private String userName = "abc";
   private String bookDescription = "bookdesc";
   private String bookGenre = "bookgenre";
   private String bookName = "bookname";
   private String bookAuthor = "bookauthor";

   @BeforeEach
   public void initMocks() {
      MockitoAnnotations.initMocks(this);
   }

   @Test
   void addBookToUser() throws ServerException, DataNotFoundException {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(new UserModel(), HttpStatus.OK));
      when(bookServiceClient.getBookById(userId)).thenReturn(new ResponseEntity<>(new BookModel(), HttpStatus.OK));
      when(libraryRepository.save(Mockito.any())).thenReturn(new UserBookData(userBookDataId, userId, bookId));
      assertEquals(new UserBookDataModel(userBookDataId, userId, bookId), libraryService.addBookToUser(userId, bookId));
   }

   @Test
   void addBookToUserDataNotFoundException() {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
      assertThrows(DataNotFoundException.class, () -> libraryService.addBookToUser(userId, bookId));
   }

   @Test
   void addBookToUserServerException() {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
      assertThrows(ServerException.class, () -> libraryService.addBookToUser(userId, bookId));
   }

   @Test
   void removeBookFromUser() throws ServerException, DataNotFoundException {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(new UserModel(), HttpStatus.OK));
      when(bookServiceClient.getBookById(userId)).thenReturn(new ResponseEntity<>(new BookModel(), HttpStatus.OK));
      when(libraryRepository.findByUserIdAndBookId(userId, bookId)).thenReturn(Optional.ofNullable(new UserBookData(userBookDataId, userId, bookId)));
      doNothing().when(libraryRepository).delete(Mockito.any());
      assertEquals(new UserBookDataModel(userBookDataId, userId, bookId), libraryService.removeBookFromUser(userId,
         bookId));
   }

   @Test
   void removeBookFromUserDataNotFoundException() {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
      assertThrows(DataNotFoundException.class, () -> libraryService.removeBookFromUser(userId, bookId));
   }

   @Test
   void removeBookFromUserServerException() {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
      assertThrows(ServerException.class, () -> libraryService.removeBookFromUser(userId, bookId));
   }

   @Test
   void getLibraryUserModel() throws ServerException, DataNotFoundException {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(new UserModel(userId, userName,
         emailId), HttpStatus.OK));
      when(libraryRepository.findAllBookIdByUserId(userId)).thenReturn(Arrays.asList(bookId));
      when(bookServiceClient.getBookById(bookId)).thenReturn(new ResponseEntity<>(new BookModel(bookId, bookName,
         bookAuthor, bookGenre, bookDescription), HttpStatus.OK));
      assertEquals(new LibraryUserModel(new UserModel(userId, userName, emailId), Arrays.asList(new BookModel(bookId,
         bookName,
         bookAuthor, bookGenre, bookDescription))), libraryService.getLibraryUserModel(userId));
   }

   @Test
   void getLibraryUserModelDataNotFoundException() {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
      assertThrows(DataNotFoundException.class, () -> libraryService.getLibraryUserModel(userId));
   }

   @Test
   void getLibraryUserModelServerException() {
      when(userServiceClient.getUserById(userId)).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
      assertThrows(ServerException.class, () -> libraryService.getLibraryUserModel(userId));
   }

   @Test
   void deleteBookById() throws ServerException, DataNotFoundException {
      when(bookServiceClient.deleteBook(bookId)).thenReturn(new ResponseEntity<>(new BookModel(bookId, bookName,
         bookAuthor, bookGenre, bookDescription), HttpStatus.OK));
      doNothing().when(libraryRepository).deleteAllByBookId(bookId);
      assertEquals(new BookModel(bookId, bookName,
         bookAuthor, bookGenre, bookDescription), libraryService.deleteBookById(bookId));
   }

   @Test
   void deleteBookByIdServerException() {
      when(bookServiceClient.deleteBook(bookId)).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
      assertThrows(ServerException.class, () -> libraryService.deleteBookById(bookId));
   }

   @Test
   void deleteBookByIdDataNotFoundException() {
      when(bookServiceClient.deleteBook(bookId)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
      assertThrows(DataNotFoundException.class, () -> libraryService.deleteBookById(bookId));
   }

   @Test
   void deleteUserById() throws ServerException, DataNotFoundException {
      when(userServiceClient.deleteUser(userId)).thenReturn(new ResponseEntity<>(new UserModel(userId, userName,
         emailId), HttpStatus.OK));
      doNothing().when(libraryRepository).deleteAllByUserId(userId);
      assertEquals(new UserModel(userId, userName, emailId), libraryService.deleteUserById(userId));
   }

   @Test
   void deleteUserByIdDataNotFoundException() {
      when(userServiceClient.deleteUser(userId)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));
      assertThrows(DataNotFoundException.class, () -> libraryService.deleteUserById(userId));
   }

   @Test
   void deleteUserByIdServerException() {
      when(userServiceClient.deleteUser(userId)).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
      assertThrows(ServerException.class, () -> libraryService.deleteUserById(userId));
   }
}
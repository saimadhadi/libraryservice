package com.epam.libraryservice.controller;

import com.epam.libraryservice.client.BookServiceClient;
import com.epam.libraryservice.client.UserServiceClient;
import com.epam.libraryservice.model.BookModel;
import com.epam.libraryservice.model.LibraryUserModel;
import com.epam.libraryservice.model.UserBookDataModel;
import com.epam.libraryservice.model.UserModel;
import com.epam.libraryservice.service.LibraryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class LibraryControllerTest {

   @Mock
   private UserServiceClient userServiceClient;

   @Mock
   private BookServiceClient bookServiceClient;

   @Mock
   private LibraryService libraryService;

   @Autowired
   private MockMvc mockMvc;

   @InjectMocks
   private LibraryController libraryController;

   private final ObjectMapper mapper = new ObjectMapper();
   private Long userId = 1L;
   private String userName = "abc";
   private String emailId = "abc@gmail.com";
   private Long bookId = 1L;
   private String bookDescription = "bookdesc";
   private String bookGenre = "bookgenre";
   private String bookName = "bookname";
   private String bookAuthor = "bookauthor";
   private Long userBookDataId = 1L;

   @BeforeEach
   public void initMocks() {
      MockitoAnnotations.initMocks(this);
      this.mockMvc = MockMvcBuilders.standaloneSetup(libraryController).build();
   }

   @Test
   void addBookToUser() throws Exception {
      when(libraryService.addBookToUser(userId, bookId)).thenReturn(new UserBookDataModel(userBookDataId, userId,
         bookId));
      mockMvc.perform(post("/library/users/1/books/1"))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON))
         .andExpect(content().json(mapper.writeValueAsString(new UserBookDataModel(userBookDataId, userId, bookId))))
         .andExpect(status().is2xxSuccessful());

   }

   @Test
   void deleteBookForUser() throws Exception {
      when(libraryService.removeBookFromUser(userId, bookId)).thenReturn(new UserBookDataModel(userBookDataId, userId
         , bookId));
      mockMvc.perform(delete("/library/users/1/books/1"))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON))
         .andExpect(content().json(mapper.writeValueAsString(new UserBookDataModel(userBookDataId, userId, bookId))))
         .andExpect(status().is2xxSuccessful());
   }

   @Test
   void getAllUsers() throws Exception {
      UserModel userModel = new UserModel(userId, userName, emailId);
      when(userServiceClient.getAllUsers()).thenReturn(new ResponseEntity<>(Arrays.asList(userModel), HttpStatus.OK));
      mockMvc.perform(get("/library/users"))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString(Arrays.asList(userModel))));
   }

   @Test
   void getLibraryUserModelById() throws Exception {
      UserModel userModel = new UserModel(userId, userName, emailId);
      BookModel bookModel = new BookModel(bookId, bookName, bookGenre, bookAuthor, bookDescription);
      when(libraryService.getLibraryUserModel(userId)).thenReturn(new LibraryUserModel(userModel,
         Arrays.asList(bookModel)));
      mockMvc.perform(get("/library/users/1"))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString((new LibraryUserModel(userModel,
            Arrays.asList(bookModel))))));
   }

   @Test
   void addUser() throws Exception {
      UserModel userModel = new UserModel(userId, userName, emailId);
      when(userServiceClient.addUser(Mockito.any())).thenReturn(new ResponseEntity<>(userModel, HttpStatus.CREATED));
      mockMvc.perform(post("/library/users").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(userModel)))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString(userModel)));
   }

   @Test
   void updateUser() throws Exception {
      UserModel userModel = new UserModel(userId, userName, emailId);
      when(userServiceClient.updateUser(Mockito.anyLong(), Mockito.any())).thenReturn(new ResponseEntity<>(userModel,
         HttpStatus.OK));
      mockMvc.perform(put("/library/users/1").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(userModel)))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString(userModel)));
   }

   @Test
   void deleteUserById() throws Exception {
      UserModel userModel = new UserModel(userId, userName, emailId);
      when(libraryService.deleteUserById(userId)).thenReturn(userModel);
      mockMvc.perform(delete("/library/users/1"))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString(userModel)));
   }

   @Test
   void getAllBooks() throws Exception {
      BookModel bookModel = new BookModel(bookId, bookName, bookGenre, bookAuthor, bookDescription);
      when(bookServiceClient.getAllBooks()).thenReturn(new ResponseEntity<>(Arrays.asList(bookModel), HttpStatus.OK));
      mockMvc.perform(get("/library/books"))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString(Arrays.asList(bookModel))));
   }

   @Test
   void getBookById() throws Exception {
      BookModel bookModel = new BookModel(bookId, bookName, bookGenre, bookAuthor, bookDescription);
      when(bookServiceClient.getBookById(bookId)).thenReturn(new ResponseEntity<>(bookModel, HttpStatus.OK));
      mockMvc.perform(get("/library/books/1"))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString(bookModel)));
   }

   @Test
   void addBook() throws Exception {
      BookModel bookModel = new BookModel(bookId, bookName, bookGenre, bookAuthor, bookDescription);
      when(bookServiceClient.addBook(bookModel)).thenReturn(new ResponseEntity<>(bookModel, HttpStatus.CREATED));
      mockMvc.perform(post("/library/books").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(bookModel)))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString(bookModel)));
   }

   @Test
   void deleteBookById() throws Exception {
      BookModel bookModel = new BookModel(bookId, bookName, bookGenre, bookAuthor, bookDescription);
      when(libraryService.deleteBookById(bookId)).thenReturn(bookModel);
      mockMvc.perform(delete("/library/books/1"))
         .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful())
         .andExpect(content().json(mapper.writeValueAsString(bookModel)));
   }
}
create table if not exists user_book_data (
	user_book_data_id bigint auto_increment,
	user_id bigint not null,
	book_id bigint not null,
	primary key(user_book_data_id)
);
package com.epam.libraryservice.service;

import com.epam.libraryservice.entity.UserBookData;
import com.epam.libraryservice.client.BookServiceClient;
import com.epam.libraryservice.client.UserServiceClient;
import com.epam.libraryservice.exception.DataNotFoundException;
import com.epam.libraryservice.exception.ServerException;
import com.epam.libraryservice.model.BookModel;
import com.epam.libraryservice.model.LibraryUserModel;
import com.epam.libraryservice.model.UserBookDataModel;
import com.epam.libraryservice.model.UserModel;
import com.epam.libraryservice.repository.LibraryRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class LibraryService {

   @Autowired
   private LibraryRepository libraryRepository;

   @Autowired
   private UserServiceClient userServiceClient;

   @Autowired
   private BookServiceClient bookServiceClient;

   private static final ObjectMapper mapper = new ObjectMapper();
   private static final Logger log = LogManager.getLogger(LibraryService.class);

   public UserBookDataModel addBookToUser(Long userId, Long bookId) throws ServerException, DataNotFoundException {
      UserBookDataModel userBookDataModel = null;
      if (getUserModelByIdFromUserService(userId) != null && getBookModelByIdFromBookService(bookId) != null) {
         UserBookData userBookData = new UserBookData();
         userBookData.setBookId(bookId);
         userBookData.setUserId(userId);
         userBookDataModel = mapper.convertValue(libraryRepository.save(userBookData), UserBookDataModel.class);
         log.info("Book : {} has been added to user : {}, {}", bookId, userId, userBookDataModel);
      }
      return userBookDataModel;
   }

   public UserBookDataModel removeBookFromUser(Long userId, Long bookId) throws DataNotFoundException, ServerException {
      UserBookData userBookData = null;
      if (getUserModelByIdFromUserService(userId) != null && getBookModelByIdFromBookService(bookId) != null) {
         userBookData =
            libraryRepository.findByUserIdAndBookId(userId, bookId).orElseThrow(() -> new DataNotFoundException("User" +
               " " +
               "does not have given book"));
         libraryRepository.delete(userBookData);
         log.info("Book : {} has been added to user : {}, {}", bookId, userId, userBookData);
      }
      return mapper.convertValue(userBookData, UserBookDataModel.class);
   }

   public LibraryUserModel getLibraryUserModel(Long userId) throws ServerException, DataNotFoundException {
      UserModel userModel = getUserModelByIdFromUserService(userId);
      List<Long> userBookIdList = libraryRepository.findAllBookIdByUserId(userId);
      List<BookModel> bookModelList = new ArrayList<>();
      for (Long bookId : userBookIdList) {
         bookModelList.add(getBookModelByIdFromBookService(bookId));
      }
      LibraryUserModel libraryUserModel = new LibraryUserModel(userModel, bookModelList);
      log.info("Library User Model for user : {}, is {}", userId, libraryUserModel);
      return libraryUserModel;
   }

   private UserModel getUserModelByIdFromUserService(Long userId) throws DataNotFoundException, ServerException {
      UserModel userModel;
      ResponseEntity<UserModel> userModelResponseEntity = userServiceClient.getUserById(userId);
      if (userModelResponseEntity.getStatusCode().is2xxSuccessful()) {
         userModel = userModelResponseEntity.getBody();
      } else if (userModelResponseEntity.getStatusCode().is4xxClientError()) {
         throw new DataNotFoundException("User not found");
      } else {
         throw new ServerException("Unable to fetch user");
      }
      return userModel;
   }

   private BookModel getBookModelByIdFromBookService(Long bookId) throws DataNotFoundException, ServerException {
      BookModel bookModel;
      ResponseEntity<BookModel> bookModelResponseEntity = bookServiceClient.getBookById(bookId);
      if (bookModelResponseEntity.getStatusCode().is2xxSuccessful()) {
         bookModel = bookModelResponseEntity.getBody();
      } else if (bookModelResponseEntity.getStatusCode().is4xxClientError()) {
         throw new DataNotFoundException("Book not found");
      } else {
         throw new ServerException("Unable to fetch book");
      }
      return bookModel;
   }

   @Transactional
   public BookModel deleteBookById(Long bookId) throws ServerException, DataNotFoundException {
      BookModel bookModel = deleteBookInBookService(bookId);
      libraryRepository.deleteAllByBookId(bookId);
      log.info("Book with id : {} is deleted", bookId);
      return bookModel;
   }

   private BookModel deleteBookInBookService(Long bookId) throws DataNotFoundException, ServerException {
      BookModel bookModel;
      ResponseEntity<BookModel> bookModelResponseEntity = bookServiceClient.deleteBook(bookId);
      if (bookModelResponseEntity.getStatusCode().is2xxSuccessful()) {
         bookModel = bookModelResponseEntity.getBody();
      } else if (bookModelResponseEntity.getStatusCode().is4xxClientError()) {
         throw new DataNotFoundException("Book not found");
      } else {
         throw new ServerException("Unable to fetch book");
      }
      return bookModel;
   }

   @Transactional
   public UserModel deleteUserById(Long userId) throws ServerException, DataNotFoundException {
      UserModel userModel = deleteUserInUserService(userId);
      libraryRepository.deleteAllByUserId(userId);
      log.info("User with id : {} is deleted", userId);
      return userModel;
   }

   private UserModel deleteUserInUserService(Long userId) throws DataNotFoundException, ServerException {
      UserModel userModel;
      ResponseEntity<UserModel> userModelResponseEntity = userServiceClient.deleteUser(userId);
      if (userModelResponseEntity.getStatusCode().is2xxSuccessful()) {
         userModel = userModelResponseEntity.getBody();
      } else if (userModelResponseEntity.getStatusCode().is4xxClientError()) {
         throw new DataNotFoundException("User not found");
      } else {
         throw new ServerException("Unable to fetch user");
      }
      return userModel;
   }
}

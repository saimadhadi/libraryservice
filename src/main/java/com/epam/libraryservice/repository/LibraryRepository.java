package com.epam.libraryservice.repository;

import com.epam.libraryservice.entity.UserBookData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface LibraryRepository extends JpaRepository<UserBookData, Long> {

   Optional<UserBookData> findByUserIdAndBookId(Long userId, Long bookId);

   @Query("select bookId from UserBookData where userId=:userId")
   List<Long> findAllBookIdByUserId(@Param("userId") Long userId);

   void deleteAllByBookId(Long bookId);

   void deleteAllByUserId(Long userId);
}

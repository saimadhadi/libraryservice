package com.epam.libraryservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "user_book_data")
public class UserBookData {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "user_book_data_id")
   private Long userBookDataId;

   @Column(name = "user_id")
   private Long userId;

   @Column(name = "book_id")
   private Long bookId;

}

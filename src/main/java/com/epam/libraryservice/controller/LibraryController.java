package com.epam.libraryservice.controller;

import com.epam.libraryservice.client.BookServiceClient;
import com.epam.libraryservice.client.UserServiceClient;
import com.epam.libraryservice.exception.DataNotFoundException;
import com.epam.libraryservice.exception.ServerException;
import com.epam.libraryservice.model.BookModel;
import com.epam.libraryservice.model.LibraryUserModel;
import com.epam.libraryservice.model.UserBookDataModel;
import com.epam.libraryservice.model.UserModel;
import com.epam.libraryservice.service.LibraryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
public class LibraryController {

   @Autowired
   private LibraryService libraryService;

   @Autowired
   private UserServiceClient userServiceClient;

   @Autowired
   private BookServiceClient bookServiceClient;

   public static final Logger log = LogManager.getLogger(LibraryController.class);

   /**
    * Add book to user
    *
    * @param bookId
    * @param userId
    * @return
    */
   @PostMapping("/library/users/{userId}/books/{bookId}")
   ResponseEntity<UserBookDataModel> addBookToUser(@PathVariable("bookId") Long bookId,
                                                   @PathVariable("userId") Long userId) {
      UserBookDataModel userBookDataModel = null;
      try {
         userBookDataModel = libraryService.addBookToUser(userId, bookId);
         log.info("Added book : {} to user : {}, {}", bookId, userId, userBookDataModel);
      } catch (ServerException e) {
         log.debug("Exception occured while adding book : {}, to user : {}", bookId, userId, e);
         throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
      } catch (DataNotFoundException e) {
         log.debug("Exception occured while adding book : {}, to user : {}", bookId, userId, e);
         throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
      }
      return new ResponseEntity<>(userBookDataModel, HttpStatus.CREATED);
   }

   /**
    * Delete book from user
    *
    * @param bookId
    * @param userId
    * @return
    */
   @DeleteMapping("/library/users/{userId}/books/{bookId}")
   ResponseEntity<UserBookDataModel> deleteBookForUser(@PathVariable("bookId") Long bookId,
                                                       @PathVariable("userId") Long userId) {
      UserBookDataModel userBookDataModel = null;
      try {
         userBookDataModel = libraryService.removeBookFromUser(userId, bookId);
         log.info("Removed book : {} from user : {}, {}", bookId, userId, userBookDataModel);
      } catch (DataNotFoundException e) {
         log.debug("Exception occured while removing book : {}, from user : {}", bookId, userId, e);
         throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
      } catch (ServerException e) {
         log.debug("Exception occured while removing book : {}, from user : {}", bookId, userId, e);
         throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
      }
      return new ResponseEntity<>(userBookDataModel, HttpStatus.CREATED);
   }

   /**
    * Get all users
    *
    * @return
    */
   @GetMapping("/library/users")
   ResponseEntity<List<UserModel>> getAllUsers() {
      log.info("Sending request to user service to get all users");
      return userServiceClient.getAllUsers();
   }

   /**
    * Get library user model by id
    *
    * @param userId
    * @return
    */
   @GetMapping("/library/users/{userId}")
   ResponseEntity<LibraryUserModel> getLibraryUserModelById(@PathVariable Long userId) {
      LibraryUserModel libraryUserModel;
      try {
         libraryUserModel = libraryService.getLibraryUserModel(userId);
         log.info("Fetched library user model with id : {}", userId);
      } catch (ServerException e) {
         log.debug("Exception occured while getting user model with id : {}", userId, e);
         throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
      } catch (DataNotFoundException e) {
         log.debug("Exception occured while getting user model with id : {}", userId, e);
         throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
      }
      return new ResponseEntity<>(libraryUserModel, HttpStatus.OK);
   }

   /**
    * Add user
    *
    * @param userModel
    * @return
    */
   @PostMapping("/library/users")
   ResponseEntity<UserModel> addUser(@RequestBody UserModel userModel) {
      log.info("Sending request to user service to add user : {}", userModel);
      return userServiceClient.addUser(userModel);
   }

   /**
    * Update user
    *
    * @param userModel
    * @return
    */
   @PutMapping("/library/users/{userId}")
   ResponseEntity<UserModel> updateUser(@PathVariable Long userId, @RequestBody UserModel userModel) {
      log.info("Sending request to user service to update user : {}", userModel);
      return userServiceClient.updateUser(userId, userModel);
   }

   @DeleteMapping("/library/users/{userId}")
   ResponseEntity<UserModel> deleteUserById(@PathVariable Long userId) {
      UserModel userModel;
      try {
         userModel = libraryService.deleteUserById(userId);
         log.info("Deleted user with id : {}, and all its associations in library", userId);
      } catch (ServerException e) {
         log.debug("Exception occured while deleting user with id : {}", userId, e);
         throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
      } catch (DataNotFoundException e) {
         log.debug("Exception occured while deleting user with id : {}", userId, e);
         throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
      }
      return new ResponseEntity<>(userModel, HttpStatus.OK);
   }

   /**
    * Get all books
    *
    * @return
    */
   @GetMapping("/library/books")
   ResponseEntity<List<BookModel>> getAllBooks() {
      log.info("Sending request to bookservice to get all books");
      return bookServiceClient.getAllBooks();
   }

   /**
    * Get book by id
    *
    * @param bookId
    * @return
    */
   @GetMapping("/library/books/{bookId}")
   ResponseEntity<BookModel> getBookById(@PathVariable Long bookId) {
      log.info("Sending request to book service to get book with id : {}", bookId);
      return bookServiceClient.getBookById(bookId);
   }

   /**
    * Add book
    *
    * @param bookModel
    * @return
    */
   @PostMapping("/library/books")
   ResponseEntity<BookModel> addBook(@RequestBody BookModel bookModel) {
      log.info("Sending request to book service to add book : {}", bookModel);
      return bookServiceClient.addBook(bookModel);
   }

   /**
    * Delete book and its associations in library
    *
    * @param bookId
    * @return
    */
   @DeleteMapping("/library/books/{bookId}")
   ResponseEntity<BookModel> deleteBookById(@PathVariable Long bookId) {
      BookModel bookModel;
      try {
         bookModel = libraryService.deleteBookById(bookId);
         log.info("Deleted book with id : {} and all its associations in library", bookId);
      } catch (ServerException e) {
         log.debug("Exception occured while deleting book with id : {}", bookId, e);
         throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
      } catch (DataNotFoundException e) {
         log.debug("Exception occured while deleting book with id : {}", bookId, e);
         throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
      }
      return new ResponseEntity<>(bookModel, HttpStatus.OK);
   }


}

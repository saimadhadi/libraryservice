package com.epam.libraryservice.client;

import com.epam.libraryservice.model.UserModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(name = "userservice")
public interface UserServiceClient {

   @GetMapping("/user")
   ResponseEntity<List<UserModel>> getAllUsers();

   @PostMapping("/user")
   ResponseEntity<UserModel> addUser(@RequestBody UserModel userModel);

   @GetMapping("/user/{userId}")
   ResponseEntity<UserModel> getUserById(@PathVariable Long userId);

   @DeleteMapping("/user/{userId}")
   ResponseEntity<UserModel> deleteUser(@PathVariable Long userId);

   @PutMapping("/user/{userId}")
   ResponseEntity<UserModel> updateUser(@PathVariable Long userId, @RequestBody UserModel userModel);
}

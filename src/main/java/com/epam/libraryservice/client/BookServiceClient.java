package com.epam.libraryservice.client;

import com.epam.libraryservice.client.fallback.BookClientFallback;
import com.epam.libraryservice.model.BookModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(name = "bookservice" , fallback = BookClientFallback.class)
public interface BookServiceClient {

   @GetMapping("/books")
   ResponseEntity<List<BookModel>> getAllBooks();

   @GetMapping("/books/{bookId}")
   ResponseEntity<BookModel> getBookById(@PathVariable Long bookId);

   @PostMapping("/books")
   ResponseEntity<BookModel> addBook(@RequestBody BookModel bookModel);

   @DeleteMapping("/books/{bookId}")
   ResponseEntity<BookModel> deleteBook(@PathVariable Long bookId);
}

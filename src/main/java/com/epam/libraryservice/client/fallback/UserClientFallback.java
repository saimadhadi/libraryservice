package com.epam.libraryservice.client.fallback;

import com.epam.libraryservice.client.UserServiceClient;
import com.epam.libraryservice.model.UserModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

public class UserClientFallback implements UserServiceClient {

   @Override
   public ResponseEntity<List<UserModel>> getAllUsers() {
      return new ResponseEntity<>(Arrays.asList(new UserModel(1L, "sample", "sample@gmail.com")), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<UserModel> addUser(UserModel userModel) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
   }

   @Override
   public ResponseEntity<UserModel> getUserById(Long userId) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
   }

   @Override
   public ResponseEntity<UserModel> deleteUser(Long userId) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
   }

   @Override
   public ResponseEntity<UserModel> updateUser(Long userId, UserModel userModel) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
   }
}

package com.epam.libraryservice.client.fallback;

import com.epam.libraryservice.client.BookServiceClient;
import com.epam.libraryservice.model.BookModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class BookClientFallback implements BookServiceClient {

   @Override
   public ResponseEntity<List<BookModel>> getAllBooks() {
      return new ResponseEntity<>(Arrays.asList(new BookModel(1L, "Head first Design patterns", "Study", "Gang of " +
         "four", "Study")), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<BookModel> getBookById(Long bookId) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
   }

   @Override
   public ResponseEntity<BookModel> addBook(BookModel bookModel) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
   }

   @Override
   public ResponseEntity<BookModel> deleteBook(Long bookId) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
   }
}

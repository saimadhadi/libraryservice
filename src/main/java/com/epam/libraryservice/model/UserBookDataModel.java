package com.epam.libraryservice.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class UserBookDataModel {

   private Long userBookDataId;
   private Long userId;
   private Long bookId;
}

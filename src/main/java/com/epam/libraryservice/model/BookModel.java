package com.epam.libraryservice.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class BookModel {
   private Long bookId;
   private String bookName;
   private String bookGenre;
   private String bookAuthor;
   private String bookDescription;
}

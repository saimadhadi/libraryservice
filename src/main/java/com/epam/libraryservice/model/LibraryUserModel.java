package com.epam.libraryservice.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class LibraryUserModel {

   private UserModel userModel;
   private List<BookModel> bookModelList;

}

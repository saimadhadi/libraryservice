package com.epam.libraryservice.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserModel {

   private Long userId;
   private String userName;
   private String emailId;
}
